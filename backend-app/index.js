var express = require("express");
var server = express();

var rssParser = require("./feedparser");

server.get('/*',function(req,res,next){
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  next();
});

server.get('/news', function(req, res){
  rssParser.getNews(function(news){
    res.send(JSON.stringify(news));
  });
});

server.get('/technology', function(req, res){
  rssParser.getTechnology(function(news){
    res.send(JSON.stringify(news));
  });
});

server.get('/music', function(req, res){
  rssParser.getMusic(function(news){
    res.send(JSON.stringify(news));
  });
});

server.get('/sport', function(req, res){
  rssParser.getSport(function(news){
    res.send(JSON.stringify(news));
  });
});

server.listen(3000, function () {
  console.log('Backend server started on port 3000');
});
