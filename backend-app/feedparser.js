var parser = require('rss-parser');

var urlRegex = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;

var interiaParser = function(entry) {
  return {
    title : entry.title,
    content : entry.contentSnippet.substring(0,100) + "...",
    link : entry.link,
    thumbnail : entry.enclosure != null ? entry.enclosure.url : "http://via.placeholder.com/350x200?text=BRAK%20MINIATURKI"
  };
};

var naszemiastoParser = function(entry) {
  return {
    title : entry.title,
    content : entry.contentSnippet.substring(0,100) + "...",
    link : entry.link,
    thumbnail : entry.content.match(urlRegex)[0] != null ? entry.content.match(urlRegex)[0] : "http://via.placeholder.com/350x200?text=BRAK%20MINIATURKI"
  };
}

var parse = function (url, parserFn, callbackFn) {
  var list = [];
  parser.parseURL(url, function(err, parsed) {
    parsed.feed.entries.forEach(entry => list.push(parserFn(entry)));
    callbackFn(list);
  });
}

module.exports = {
  getNews: function(callbackFn){
    var newsList = [];
    parse("http://krakow.naszemiasto.pl/rss/artykuly/1086.xml", naszemiastoParser, function(result){
      newsList = newsList.concat(result);
      parse("http://fakty.interia.pl/feed", interiaParser, function(result){
        newsList = newsList.concat(result);
        callbackFn(newsList);
      });
    })
  },
  getTechnology: function(callbackFn){
    parse("http://nt.interia.pl/feed", interiaParser, function(newsList){
      callbackFn(newsList);
    });
  },
  getMusic: function(callbackFn){
    var newsList = [];
    parse("http://krakow.naszemiasto.pl/rss/artykuly/402.xml", naszemiastoParser, function(result){
      newsList = newsList.concat(result);
      parse("http://muzyka.interia.pl/feed", interiaParser, function(result){
        newsList = newsList.concat(result);
        callbackFn(newsList);
      });
    });
  },
  getSport: function(callbackFn){
    var newsList = [];
    parse("http://krakow.naszemiasto.pl/rss/artykuly/3.xml", naszemiastoParser, function(result){
      newsList = newsList.concat(result);
      parse("http://sport.interia.pl/feed", interiaParser, function(result){
        newsList = newsList.concat(result);
        callbackFn(newsList);
      });
    });
  }
}
