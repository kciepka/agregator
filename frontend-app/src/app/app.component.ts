import { Component } from '@angular/core';
import { News } from './news';
import { NewsFeedService } from './news-feed.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NewsFeedService]
})
export class AppComponent {

  constructor(private newsFeedService : NewsFeedService){ this.getArticles("news"); }

  articles : News[];

  getArticles(type) : void {
    this.newsFeedService.getArticles(type).then(articles => this.articles = articles);
  };
}
