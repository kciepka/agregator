import { Injectable } from '@angular/core';
import { News } from './news';
import 'rxjs/add/operator/toPromise';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class NewsFeedService {

  constructor(private http: HttpClient) { }

private handleError(error: any): Promise<any> {
  return Promise.reject(error.message || error);
}

  getArticles(type) : Promise<News[]> {
    return this.http.get('http://localhost:3000/' + type)
    .toPromise()
    .then(response => {
       return response as News[];
    })
    .catch(this.handleError);
  }
}
