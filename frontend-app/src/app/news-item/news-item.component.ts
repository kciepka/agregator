import { Component, Input } from '@angular/core';
import { News } from '../news';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent{

  @Input() article : News;

  constructor() { }

}
