export class News {
  title : string;
  content : string;
  thumbnail : string;
  link : string;
}
